# Howto setup Hemodialysis Report

## Introduction

Hemodialysis Report is ...

Example
![](https://pbs.twimg.com/media/DjiRTJ6XcAEq3WE.jpg:medium)

## Development

### Technology & Tools

- Spring 4
- AngularJS version ?
- HTML 5 / CSS 3
- Java 8
- Maven
- MySQL version ?
- Git
- IntelliJ IDEA
- [DBveaver](https://dbeaver.io)

### Project Structure (Example)

- eforl_core - https://bitbucket.org/theerasun_tangngamchit/eforl_core
- nicu_report - https://bitbucket.org/theerasun_tangngamchit/nicu_report

### Setup

#### 1. Clone projects and update Maven

```
# eforl_core
git clone git@bitbucket.org:theerasun_tangngamchit/eforl_core.git
cd eforl_core
mvn verify install

# nicu_report
git clone git@bitbucket.org:theerasun_tangngamchit/nicu_report.git
cd nicu_report
mvn verify
mvn test
```
#### 2. Setup IDE
- Import Project to IntelliJ IDEA
- Setup Profile
- Setup Run

#### 3. Setup Database
- Config db username and password
- Start MySQL

```
# START
docker-compose up -d

# VIEW LOGS
docker-compose logs -f

# SSH
docker exec -it docker_db_1 /bin/bash
 
# STOP
docker-compose down
```

- Run init script
- Run migrate data script

#### 4. Development Flow
- Code
- Unit Test
- Integration Test (Cypress)

more...

## Notes
 - [MySQL Public Key Retrieval is not allowed](https://community.atlassian.com/t5/Confluence-questions/MySQL-Public-Key-Retrieval-is-not-allowed/qaq-p/778956)
